/* BOM: son objetos usados para interactuar con el user. Se necesita una función para ejecutar. SIEMPRE FIJARSE QUE OBJETOS SON COMPATIBLES CON EL NAVEGADOR, LOS OBJETOS DEL BOM SON SIEMPRE LOS MENOS COMPATIBLES CON LOS NAVEGADORES */

function saludar() {
    alert("Hola mundo");
}

// setTimeout(nombreDeLaFuncionAEjecutar, tiempoEnMilisegundosEnElQueSeEjecutaraLaFuncion): método para ejecutar una función después de 4 segundos
// window.setTimeout(saludar, 4000);

// setInterval: ejecuta una función cada determinado tiempo, en milisegundos
// window.setInterval(saludar, 3000);

// window.setInterval(contar, 1000);
let tiempo = window.setInterval(contar, 1000);

let contador = 1;

function contar() {
    document.write(contador + "<br>");
    // contador++;
    if (contador == 10) {
        window.clearInterval(tiempo);   // clearInterval(): método para limpiar el intervalo de tiempo, es decir, para detener la ejecución de una función.
    }
    contador++;
}



