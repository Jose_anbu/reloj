function obtenerHora() {

    let fecha = new Date();
    // console.log(fecha);
    // console.log(fecha.getDate()); // nro de día del mes
    // console.log(fecha.getDay()); // nro del día de la semana, empieza desde el domingo con el nro 0
    // console.log(fecha.getMonth()); // nro del mes, inicia en Enero con el nro 0

    let pDiaSemana = document.getElementById(`diaSemana`),
        pDia = document.getElementById(`dia`),
        pMes = document.getElementById(`mes`),
        pAnio = document.getElementById(`anio`),
        pHoras = document.getElementById(`horas`),
        pMinutos = document.getElementById(`minutos`),
        pSegundos = document.getElementById(`segundos`),
        pAmPm = document.getElementById(`amPm`);

    let diasReloj = ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"];
    let meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];

    pDiaSemana.innerText = diasReloj[fecha.getDay()];
    pDia.innerText = fecha.getDate();
    pMes.innerText = meses[fecha.getMonth()];
    pAnio.innerText = fecha.getFullYear();


    if (fecha.getHours() >= 12) {
        pAmPm.innerText = "pm";
        pHoras.innerText = fecha.getHours() - 12; // pHoras.innerText = "0" + (fecha.getHours() - 12);

        if (parseInt(pHoras.innerText) == 0) {
            pHoras.innerText = 12;
        }

    } else {
        pAmPm.innerText = "am";
        pHoras.innerText = fecha.getHours(); // pHoras.innerText = "0" + fecha.getHours();
    }

    if (parseInt(pHoras.innerText) < 10) {          // SI USAMOS LOS CODIGOS COMENTADOS DE LINEAS 29 Y 32 SE PUEDE ELIMINAR ESTE IF
        pHoras.innerText = "0" + pHoras.innerText;
    }

    if (fecha.getSeconds() < 10) {
        pSegundos.innerText = "0" + fecha.getSeconds();
    } else {
        pSegundos.innerText = fecha.getSeconds();
    }

    if (fecha.getMinutes() < 10) {
        pMinutos.innerText = "0" + fecha.getMinutes();
    } else {
        pMinutos.innerText = fecha.getMinutes();
    }
}

window.setInterval(obtenerHora, 1000);
